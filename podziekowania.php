<!DOCTYPE html>
<html>
<head>
    <title>Podziękowania</title>
</head>
<body>
    <h1>Dziękujemy za wypełnienie ankiety!</h1>
    <h2>Podsumowanie odpowiedzi:</h2>

    <?php
    $mysqli = new mysqli("localhost", "root", "", "pain_php");

    if ($mysqli->connect_error) {
        die("Błąd połączenia z bazą danych: " . $mysqli->connect_error);
    }

    $query = "SELECT pyt.id, pyt.pytania, 
                     SUM(CASE WHEN ou.odpowiedz = 'a' THEN 1 ELSE 0 END) AS ilosc_odpowiedzi_a,
                     SUM(CASE WHEN ou.odpowiedz = 'b' THEN 1 ELSE 0 END) AS ilosc_odpowiedzi_b,
                     SUM(CASE WHEN ou.odpowiedz = 'c' THEN 1 ELSE 0 END) AS ilosc_odpowiedzi_c,
                     SUM(CASE WHEN ou.odpowiedz = 'd' THEN 1 ELSE 0 END) AS ilosc_odpowiedzi_d
              FROM pytania pyt
              LEFT JOIN odpowiedzi_uzytkownikow ou ON pyt.id = ou.pytanie_id
              GROUP BY pyt.id, pyt.pytania";
    
    $result = $mysqli->query($query);

    if ($result->num_rows > 0) {
        echo "<ul>";
        while ($row = $result->fetch_assoc()) {
            $pytanie = $row['pytania'];
            $ilosc_odpowiedzi_a = $row['ilosc_odpowiedzi_a'];
            $ilosc_odpowiedzi_b = $row['ilosc_odpowiedzi_b'];
            $ilosc_odpowiedzi_c = $row['ilosc_odpowiedzi_c'];
            $ilosc_odpowiedzi_d = $row['ilosc_odpowiedzi_d'];

            echo "<li>$pytanie:
                  Odpowiedź A: $ilosc_odpowiedzi_a razy,
                  Odpowiedź B: $ilosc_odpowiedzi_b razy,
                  Odpowiedź C: $ilosc_odpowiedzi_c razy,
                  Odpowiedź D: $ilosc_odpowiedzi_d razy</li>";
        }
        echo "</ul>";
    } else {
        echo "Brak odpowiedzi w bazie danych.";
    }

    $mysqli->close();
    ?>

</body>
</html>

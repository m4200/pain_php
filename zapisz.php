<?php
$mysqli = new mysqli("localhost", "root", "", "pain_php");

if ($mysqli->connect_error) {
    die("Błąd połączenia z bazą danych: " . $mysqli->connect_error);
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    $odpowiedzi = $_POST["odpowiedzi"];
    
    foreach ($odpowiedzi as $pytanie_id => $odpowiedz) {
        $query = "INSERT INTO odpowiedzi_uzytkownikow (pytanie_id, odpowiedz) VALUES ($pytanie_id, '$odpowiedz')";
        $result = $mysqli->query($query);
        
        if (!$result) {
            echo "Błąd SQL: " . $mysqli->error;
            echo "Zapytanie SQL: " . $query;
        }
    }
}

$mysqli->close();

header("Location: podziekowania.php");
?>

<!DOCTYPE html>
<html>
<head>
    <title>Ankieta programistyczna</title>
</head>
<body>
    <h1>Ankieta programistyczna</h1>
    <form action="zapisz.php" method="post">
        <?php
            $mysqli = new mysqli("localhost", "root", "", "pain_php");

            if ($mysqli->connect_error) {
                die("Błąd połączenia z bazą danych: " . $mysqli->connect_error);
            }

            $query = "SELECT * FROM pytania";
            $result = $mysqli->query($query);

            while ($row = $result->fetch_assoc()) {
                $pytanie_id = $row['id'];
                $tresc_pytania = $row['pytania'];
                echo "<p>$tresc_pytania</p>";
                
                $query_odpowiedzi = "SELECT * FROM odp_pyt WHERE do_pyt = $pytanie_id";
                $result_odpowiedzi = $mysqli->query($query_odpowiedzi);
                $options = ['a', 'b', 'c', 'd'];
                $i = 0;
                
                while ($row_odpowiedzi = $result_odpowiedzi->fetch_assoc()) {
                    $odpowiedz_id = $row_odpowiedzi['id'];
                    $tresc_odpowiedzi = $row_odpowiedzi['tresc_pyt'];
                    $input_name = "odpowiedzi[$pytanie_id]";
                    $input_value = $options[$i];
                    echo "<label><input type='radio' name='$input_name' value='$input_value'>$tresc_odpowiedzi</label><br>";
                    $i++;
                }
            }

            $mysqli->close();
        ?>
        <input type="submit" value="Prześlij">
    </form>
</body>
</html>
